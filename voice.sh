#!/usr/bin/env zsh

now="$(date +%H:%M)"

serverAddr="01.lpd-net.ru"
serverPort=10024

Version=2014004
CallsignAndUser="OE1ZBQ, Illia"
EMailAddress="ut3uqz@gmail.com"
BandAndChannel="PC Only"
Description="Test"
Country="Austria"
CityCityPart="Vienna - JN88ee"
DynPassWord="LTWORBEE"
Net="Russia"

echo "$now -- CT:<VX>[$Version]</VX><EA>[$EMailAddress]</EA><PW>[$DynPassWord]</PW><ON>[$CallsignAndUser]</ON><CL>[$ClientType]</CL><BC>[$BandAndChannel]</BC><DS>[$Description]</DS><NN>[$Country]</NN><CT>[$CityCityPart]</CT><NT>[$Net]</NT>"
(echo "CT:<VX>[$Version]</VX><EA>[$EMailAddress]</EA><PW>[$DynPassWord]</PW><ON>[$CallsignAndUser]</ON><CL>[$ClientType]</CL><BC>[$BandAndChannel]</BC><DS>[$Description]</DS><NN>[$Country]</NN><CT>[$CityCityPart]</CT><NT>[$Net]</NT>") | nc $serverAddr $serverPort
(echo "P") | nc $serverAddr $serverPort
