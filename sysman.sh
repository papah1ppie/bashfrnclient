#!/usr/bin/env zsh

now="$(date +%H:%M)"

serverAddr="sysman.freeradionetwork.eu"
serverPort=10025

CallsignAndUser="OE1ZBQ, Illia"
EMailAddress="ut3uqz@gmail.com"
BandAndChannel="PC Only"
Description="Test"
Country="Austria"
CityCityPart="Vienna - JN88ee"


echo "$now -- IG:<ON>['$CallsignAndUser']</ON><EA>['$EMailAddress']</EA><BC>['$BandAndChannel']</BC><DS>['$Description']</DS><NN>['$Country']</NN><CT>['$CityCityPart']</CT>"
(echo "IG:<ON>['$CallsignAndUser']</ON><EA>['$EMailAddress']</EA><BC>['$BandAndChannel']</BC><DS>['$Description']</DS><NN>['$Country']</NN><CT>['$CityCityPart']</CT>") | nc $serverAddr $serverPort

